# MemeNoir
For Africa and Africa Diaspora

# Features:
- Free Meme Templates which includes: Humans, Cartoons, Animals and Others
- Use Picture From Phone Camera on MemeNoir
- Use Picture From Gallery on MemeNoir
- Create Meme
- Save Created Meme
- Share Created Meme on Social Media
- Read And Follow MemeNoir Blog
- Support for Android API 14+


##Screenshots

###Android 5.1

![Alt text](/screenshots/1.png?raw=true "MemeNoir")
![Alt text](/screenshots/2.png?raw=true "MemeNoir")
![Alt text](/screenshots/3.png?raw=true "MemeNoir")
![Alt text](/screenshots/4.png?raw=true "MemeNoir")


#Developed By

* Silas Okwoche and Anuoluwapo Wahab, for Nerve Mobile Systems - http://nerveflo.com
