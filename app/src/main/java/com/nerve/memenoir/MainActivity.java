package com.nerve.memenoir;


import android.*;
import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nerve.memenoir.Blog.Activity.BlogActivity;
import com.nerve.memenoir.Blog.Activity.BlogPostDetailActivity;
import com.nerve.memenoir.Blog.Data.Post;
import com.nerve.memenoir.Blog.Utils.ActivityUtils;
import com.nerve.memenoir.models.updateModel;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private int mCurrentSelectedPosition;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CAMERA_REQUEST = 1888;
    ViewPager vpPager;
    SlidingTabLayout slidingTabLayout;
    FrameLayout _container;
    AdRequest adRequest;
    AdView mAdViewBanner;
    SharedPreferences appSharedPrefs;
    DatabaseReference mFirebaseDatabaseReference;
    MenuItem notificationMenu;
    private Dialog b;
    private TextView _instruction;
    private Button _cancel, _retry;
    private ProgressBar _progressBar;
    private LinearLayout _action;
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpNavigationDrawer();

        //FirebaseMessaging.getInstance().subscribeToTopic("MemeNoir");

        vpPager = (ViewPager) findViewById(R.id.vpPager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        _container = (FrameLayout)findViewById(R.id.container);

        appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        // Initial tab count
        setTabs(4);
        mNavigationView.setCheckedItem(R.id.navigation_item_1);
        //AdMob Banner
        mAdViewBanner = (AdView) findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        mAdViewBanner.loadAd(adRequest);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        //Check for updates
        checkForUpdate();

        final SharedPreferences pref = MainActivity.this.getSharedPreferences("Memenoir", MODE_PRIVATE);
        //If not admin, check for user's admin status
        if (!pref.getBoolean("isAdmin", false)){
            // Check if the READ_PHONE_STATE permission is already available.
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                // READ_PHONE_STATE permission has not been granted.
                requestReadPhoneStatePermission();
            } else {
                // READ_PHONE_STATE permission is already been granted.
                //check if user is an admin
                checkIfAdmin();
            }
        }else{
            //is an admin
            Menu nav_Menu = mNavigationView.getMenu();
            nav_Menu.findItem(R.id.navigation_item_5).setVisible(true);
        }

        //OPEN BLOG
        Bundle extras = getIntent().getExtras();
        try {
            final String url = extras.getString("url");
            String search = "public-api.wordpress.com/rest/v1/sites/";
            if (url != null && url.contains(search)) {
                //It's a blog, hence open blog
                //Show a dialog while preparing the blog activity
                b = new Dialog(MainActivity.this, R.style.Alert);
                b.requestWindowFeature(Window.FEATURE_NO_TITLE);
                b.setTitle("ATTENTION!!!");
                b.setCancelable(true);
                b.setContentView(R.layout.blog_spinner);
                _retry = (Button) b.findViewById(R.id.retry);
                _cancel = (Button) b.findViewById(R.id.cancel);
                _action = (LinearLayout) b.findViewById(R.id.action);
                _instruction = (TextView) b.findViewById(R.id.instruction);
                _progressBar = (ProgressBar) b.findViewById(R.id.progressBar);
                consumeUrl(url);

                _cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        b.cancel();
                    }
                });
                _retry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        consumeUrl(url);
                        _action.setVisibility(View.GONE);
                        _instruction.setText("Loading, please wait...");
                        _progressBar.setVisibility(View.VISIBLE);
                    }
                });

                //SHOW
                b.show();
            }
        }catch(Exception e){}
    }

    private void consumeUrl(String url) {
        Log.i(TAG, "consumeUrl " + url);
        JsonObjectRequest objectRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "onResponse");
                parserRequest(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse");
                _action.setVisibility(View.VISIBLE);
                _instruction.setText("Oops! Check internet connection and hit the retry button.");
                _progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                    headers.put("app", "NtczZTk4MDIxMTVlNjYx");
                    headers.put("hash", "26f8d06051d593efb9623d54c12eb2f0b158df35");
                    headers.put("Authorization", "Basic " + "ZnVubWluaXlpZWt1bmRheW9AZ21haWwuY29tOnF3ZXJ0eQ==");
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(7000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        objectRequest.setShouldCache(true);
        objectRequest.setTag(TAG);
        App.getInstance().getRequestQueue().add(objectRequest);
    }

    private void parserRequest(JSONObject object) {
        Log.i(TAG, "parserRequest with " + object.toString());
        Log.d(TAG, "parserRequest: Blog");
        BlogPostDetailActivity.mPost = new Post(object);
        Intent intent = new Intent(this, BlogPostDetailActivity.class);
        startActivity(intent);
        b.dismiss();
    }

    private void requestReadPhoneStatePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Permission Request")
                    .setMessage("Permission required!")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //re-request
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    1);
                        }
                    })
                    .setIcon(R.mipmap.ic_launcher)
                    .show();
        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == 1) {
            // Received permission result for READ_PHONE_STATE permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // READ_PHONE_STATE permission has been granted, proceed with displaying IMEI Number
                //alertAlert(getString(R.string.permision_available_read_phone_state));
                checkIfAdmin();
            } else {
                alertAlert("Permission required!");
            }
        }
    }

    private void alertAlert(String msg) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Permission Request")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do somthing here
                    }
                })
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }

    private void setUpNavigationDrawer() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        try {
            assert actionBar != null;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setSubtitle(getString(R.string.subtitle));
            actionBar.setDisplayShowTitleEnabled(true);
        } catch (Exception ignored) {
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        Menu nav_Menu = mNavigationView.getMenu();
        nav_Menu.findItem(R.id.navigation_item_5).setVisible(false);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.navigation_item_1:
                        slidingTabLayout.setVisibility(View.VISIBLE);
                        vpPager.setVisibility(View.VISIBLE);
                        _container.setVisibility(View.GONE);
                        mCurrentSelectedPosition = 4;
                        setTabs(mCurrentSelectedPosition);
                        break;
                    case R.id.navigation_item_2:
                        mDrawerLayout.closeDrawer(mNavigationView);
                        slidingTabLayout.setVisibility(View.GONE);
                        vpPager.setVisibility(View.GONE);
                        _container.setVisibility(View.VISIBLE);
                        FragmentManager fm = getSupportFragmentManager();
                        Saved_Meme fragment = new Saved_Meme();
                        fm.beginTransaction().add(R.id.container,fragment).commit();
                        break;
                    case R.id.navigation_item_3:
                        Intent blog = new Intent(MainActivity.this, BlogActivity.class);
                        startActivity(blog);
                        break;
                    case R.id.navigation_item_4:
                        About();
                        break;

                    case R.id.navigation_item_5:
                        Intent intent = new Intent(MainActivity.this, Upload.class);
                        startActivity(intent);
                        break;
                }

                //setTabs(mCurrentSelectedPosition + 1);
                mDrawerLayout.closeDrawer(mNavigationView);
                return true;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle(getString(R.string.drawer_opened));
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    public void setTabs(int count) {
        ContentFragmentAdapter adapterViewPager = new ContentFragmentAdapter(getSupportFragmentManager(), this, count);
        vpPager.setAdapter(adapterViewPager);

        slidingTabLayout.setTextColor(getResources().getColor(R.color.tab_text_color));
        slidingTabLayout.setTextColorSelected(getResources().getColor(R.color.tab_text_color_selected));
        slidingTabLayout.setDistributeEvenly();
        slidingTabLayout.setViewPager(vpPager);
        slidingTabLayout.setTabSelected(0);

        // Change indicator color
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tab_indicator);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mNavigationView)) {
            mDrawerLayout.closeDrawer(mNavigationView);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //MenuItem searchItem = menu.findItem(R.id.action_search);
        notificationMenu = menu.findItem(R.id.action_notif);

        //Subscribe to topic "MemeNoir"
        if (appSharedPrefs.getBoolean("isNotifOn",true)){
            //By default turn it ON
            FirebaseMessaging.getInstance().subscribeToTopic("MemeNoir");
            notificationMenu.setIcon(getResources().getDrawable(R.drawable.notif_open));
        }else{
            //Notif is OFF
            FirebaseMessaging.getInstance().unsubscribeFromTopic("MemeNoir");
            notificationMenu.setIcon(getResources().getDrawable(R.drawable.notif_closed));
        }

    /*    if (searchItem != null) {
            SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

            // use this method for search process
            searchView.setOnQueryTextListener(new_tag SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    // use this method when query submitted
                    Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    // use this method for auto complete search process
                    return false;
                }
            });

        }*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_gallery:
                openGallery();
                return true;
            case R.id.action_camera:
                openCamera();
                return true;
            case R.id.action_notif:
                turnNofit();
                return true;
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mNavigationView)) {
                    mDrawerLayout.closeDrawer(mNavigationView);
                } else {
                    mDrawerLayout.openDrawer(mNavigationView);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public  void turnNofit(){
        try {
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            if (appSharedPrefs.getBoolean("isNotifOn", true)) {
                //Notif is ON, then turn it OFF
                FirebaseMessaging.getInstance().unsubscribeFromTopic("MemeNoir");
                notificationMenu.setIcon(getResources().getDrawable(R.drawable.notif_closed));
                Toast.makeText(this, "Notification is turned OFF", Toast.LENGTH_SHORT).show();
                prefsEditor.putBoolean("isNotifOn", false);
                prefsEditor.apply();
            } else {
                //Notif is OFF, then turn it ON
                FirebaseMessaging.getInstance().subscribeToTopic("MemeNoir");
                notificationMenu.setIcon(getResources().getDrawable(R.drawable.notif_open));
                Toast.makeText(this, "Notification is turned ON", Toast.LENGTH_SHORT).show();
                prefsEditor.putBoolean("isNotifOn", true);
                prefsEditor.apply();
            }
        }catch (Exception e){}

    }

    public void openGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,RESULT_LOAD_IMAGE);
    }

    public void openCamera(){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null,null,null);
            cursor.moveToFirst();
            Toast.makeText(this, "done", Toast.LENGTH_SHORT).show();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            Intent startEditActivity = new Intent(this, EditActivity.class);
            startEditActivity.putExtra("filePath", picturePath);
            startActivity(startEditActivity);
        }else if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK && data != null){
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Intent startEditActivity = new Intent(this, EditActivity.class);
                startEditActivity.putExtra("cameraBitmap", photo);
                startActivity(startEditActivity);
            }
        }

        public void checkIfAdmin() {
            try {
                final SharedPreferences pref = MainActivity.this.getSharedPreferences("Memenoir", MODE_PRIVATE);
                final SharedPreferences.Editor editor = pref.edit();
                TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String device_id = tm.getDeviceId();
                mFirebaseDatabaseReference.child("Admin IMEI")
                        .child(device_id.trim()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        System.out.println(snapshot.getValue());
                        if (snapshot.exists()) {
                            //Go into the App
                            Menu nav_Menu = mNavigationView.getMenu();
                            nav_Menu.findItem(R.id.navigation_item_5).setVisible(true);
                            Toast.makeText(MainActivity.this, "You are an admin", Toast.LENGTH_SHORT).show();
                            editor.putBoolean("isAdmin",true);
                            editor.apply();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        //Display network error message
                    }
                });
            }catch (Exception e){}
        }

        public void About(){
            //showDialog();
            final Dialog d = new Dialog(MainActivity.this, R.style.Alert);
            d.requestWindowFeature(Window.FEATURE_NO_TITLE);
            d.setTitle("ATTENTION!!!");
            d.setCancelable(true);
            d.setContentView(R.layout.about);
            TextView developed_by = (TextView) d.findViewById(R.id.developed_by);
            Linkify.addLinks(developed_by, Linkify.WEB_URLS);
            //developed_by.setMovementMethod(LinkMovementMethod.getInstance());

            //SHOW
            d.show();
        }

    public void checkForUpdate(){
        mFirebaseDatabaseReference.child("Latest Update").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                System.out.println(snapshot.getValue());
                if (snapshot.exists()) {
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        try {
                            if (postSnapshot.getKey().equals("Version Configuration")) {
                                final updateModel update = postSnapshot.getValue(updateModel.class);
                                int versionCode = BuildConfig.VERSION_CODE;
                                if (Integer.parseInt(update.getVersionCode()) > versionCode) {
                                    final Dialog d = new Dialog(MainActivity.this, R.style.Alert);
                                    d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    d.setCancelable(true);
                                    d.setContentView(R.layout.update);
                                    TextView whatsNew = (TextView) d.findViewById(R.id.whatsNew);
                                    whatsNew.setText(update.getWhatsNew());

                                    Button updateApp = (Button) d.findViewById(R.id.updateApp);
                                    Button cancel = (Button) d.findViewById(R.id.cancel);
                                    updateApp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //String url = "https://play.google.com/store/apps/details?id=com.nerve.memenoir";
                                            Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(update.getUrl()));
                                            Intent chooser = Intent.createChooser(sendIntent, "Continue with...");
                                            if (sendIntent.resolveActivity(getPackageManager()) != null) {
                                                startActivity(chooser);
                                                d.dismiss();
                                            }
                                        }
                                    });
                                    cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            d.dismiss();
                                        }
                                    });

                                    //SHOW
                                    d.show();
                                }
                            }
                        }catch (Exception e){}
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                //Display network error message
            }
        });
    }
}
