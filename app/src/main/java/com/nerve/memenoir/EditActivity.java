package com.nerve.memenoir;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import yuku.ambilwarna.AmbilWarnaDialog;

public class EditActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView _editMeme, _rotate;
    LinearLayout _editBar, _tools;
    EditText _createCaption;
    ImageButton _doneCaption;
    TextView _bottomText, _topText;
    View _lay, _colorHolder;
    SeekBar _padding;
    Boolean isEditingBottomText = false, isEditingTopText = false, memeSaved = false, memeSavedNow = false, interstitialShown = false, isClickedImage = false;
    String caption, filename, dirPath;
    Float font, bottomTextSize, topTextSize;
    Spinner _fontSize;
    AdRequest adRequest;
    InterstitialAd mInterstitialAd, mInterstitialAdVideo;
    AdView mAdViewBanner;
    int progressValue = 0, BackgroundChosenColor = Color.GRAY, TopChosenColor = Color.WHITE, BottomChosenColor = Color.WHITE, rotationPosition = 0;
    private static final String[]paths = {"Size","18", "20", "22","24", "26", "28","30", "32", "34","36", "38", "40","42", "44", "46","48", "50"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        try {
            assert actionBar != null;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setSubtitle(getString(R.string.subtitle));
            actionBar.setDisplayShowTitleEnabled(true);
        } catch (Exception ignored) {}

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/impact.ttf");
        _editMeme = (ImageView)findViewById(R.id.editMeme);
        _bottomText = (TextView) findViewById(R.id.bottomText);
        _bottomText.setTypeface(typeface);
        _topText = (TextView) findViewById(R.id.topText);
        _topText.setTypeface(typeface);
        _editBar = (LinearLayout)findViewById(R.id.edit_bar);
        _doneCaption = (ImageButton) findViewById(R.id.done_caption);
        _createCaption = (EditText)findViewById(R.id.create_caption);
        _fontSize = (Spinner)findViewById(R.id.font_size);
        _lay = (View)findViewById(R.id.lay);
        _rotate = (ImageView)findViewById(R.id.rotate);
        _padding = (SeekBar)findViewById(R.id.padding);
        _colorHolder = (View)findViewById(R.id.color_holder);
        _tools = (LinearLayout)findViewById(R.id.tools);

        //Hide keyboard
        hideKeyboard(EditActivity.this);

        //AdMob Banner
        //MobileAds.initialize(getApplicationContext(), getString(R.string.google_ad_id_banner));
        mAdViewBanner = (AdView) findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        mAdViewBanner.loadAd(adRequest);

        //Admob Interstitial
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.google_ad_id_intastitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        //Admob Interstitial
        mInterstitialAdVideo = new InterstitialAd(this);
        mInterstitialAdVideo.setAdUnitId(getString(R.string.google_ad_id_intastitial_video));
        mInterstitialAdVideo.loadAd(new AdRequest.Builder().build());


        try {
            String title = getIntent().getStringExtra("name");
            final String image = getIntent().getStringExtra("image");
            final String filePath = getIntent().getStringExtra("filePath");
            Intent intent = getIntent();
            Bitmap bitmap = (Bitmap) intent.getParcelableExtra("cameraBitmap");
            if (filePath != null) {  //from gallery
                _editMeme.setImageBitmap(BitmapFactory.decodeFile(filePath));
            } else if (bitmap != null) {  //from camera
                _editMeme.setImageBitmap(bitmap);
            } else {  //from home
                ImageLoader.getInstance().displayImage(image, _editMeme);
            }
        }catch (Exception e){}

        //Get the padding set by user
        _padding.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = (progress*5);
                float scale = getResources().getDisplayMetrics().density;
                int paddingAsPixels = (int) (progressValue*scale + 0.5f);
                _editMeme.setPadding(paddingAsPixels,paddingAsPixels,paddingAsPixels,paddingAsPixels);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //Rotation image n button click
        _rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rotationPosition == 0) {
                    _editMeme.setRotation(90);
                    rotationPosition = 90;
                } else if (rotationPosition == 90) {
                    _editMeme.setRotation(180);
                    rotationPosition = 180;
                } else if (rotationPosition == 180) {
                    _editMeme.setRotation(270);
                    rotationPosition = 270;
                } else {
                    _editMeme.setRotation(360);
                    rotationPosition = 0;
                }
            }
        });

        //Color picker
        _colorHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClickedImage){
                    AmbilWarnaDialog ambilWarnaDialog = new AmbilWarnaDialog(EditActivity.this, BackgroundChosenColor, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog ambilWarnaDialog, int color) {
                            BackgroundChosenColor = color;
                            _editMeme.setBackgroundColor(color);
                            _colorHolder.setBackgroundColor(color);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog ambilWarnaDialog) {
                        }
                    });
                    ambilWarnaDialog.show();
                }
                if (isEditingTopText){
                    AmbilWarnaDialog ambilWarnaDialog = new AmbilWarnaDialog(EditActivity.this, TopChosenColor, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog ambilWarnaDialog, int color) {
                            TopChosenColor = color;
                            _topText.setTextColor(color);
                            _colorHolder.setBackgroundColor(color);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog ambilWarnaDialog) {
                        }
                    });
                    ambilWarnaDialog.show();
                }
                if (isEditingBottomText){
                    AmbilWarnaDialog ambilWarnaDialog = new AmbilWarnaDialog(EditActivity.this, BottomChosenColor, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog ambilWarnaDialog, int color) {
                            BottomChosenColor = color;
                            _bottomText.setTextColor(color);
                            _colorHolder.setBackgroundColor(color);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog ambilWarnaDialog) {
                        }
                    });
                    ambilWarnaDialog.show();
                }
            }
        });

        ArrayAdapter<String>adapter = new ArrayAdapter<String>(EditActivity.this, android.R.layout.simple_spinner_item,paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _fontSize.setAdapter(adapter);
        _fontSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!parent.getItemAtPosition(position).toString().equals("Size")) {
                    font = Float.valueOf(parent.getItemAtPosition(position).toString());
                        if (isEditingBottomText) {
                            _bottomText.setTextSize(font);
                            bottomTextSize = font;
                        } else {
                            _topText.setTextSize(font);
                            topTextSize = font;
                        }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                font = Float.valueOf("18");
            }
        });

        //Default font size
        font = Float.valueOf("24");
        _bottomText.setTextSize(font);
        _topText.setTextSize(font);

        _editMeme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isClickedImage = true;
                isEditingBottomText = false;
                isEditingTopText = false;
                _colorHolder.setBackgroundColor(BottomChosenColor);
            }
        });

        _bottomText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _editBar.setVisibility(View.VISIBLE);
                _tools.setVisibility(View.VISIBLE);
                isEditingBottomText = true;
                isEditingTopText = false;
                isClickedImage = false;
                _colorHolder.setBackgroundColor(BottomChosenColor);
                if (!_bottomText.getText().toString().trim().equals("Tap Here to Add Caption")){
                    _createCaption.setText(_bottomText.getText().toString().trim());
                }else {
                    _createCaption.setText("");
                }
                _createCaption.requestFocus();
                showKeyboard(EditActivity.this);
            }
        });
        _topText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _editBar.setVisibility(View.VISIBLE);
                _tools.setVisibility(View.VISIBLE);
                isEditingTopText = true;
                isEditingBottomText = false;
                isClickedImage = false;
                _colorHolder.setBackgroundColor(TopChosenColor);
                if (!_topText.getText().toString().trim().equals("Tap Here to Add Caption")){
                    _createCaption.setText(_topText.getText().toString().trim());
                }else {
                    _createCaption.setText("");
                }
                _createCaption.requestFocus();
                showKeyboard(EditActivity.this);
            }
        });


        _createCaption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SpannableString spannableString = new SpannableString(s);
                if (isEditingBottomText) {
                    if (!_createCaption.getText().toString().equals("")) {
                        _bottomText.setText(spannableString);
                    }
                } else {
                    if (!_createCaption.getText().toString().equals("")) {
                        _topText.setText(spannableString);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        _doneCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                caption = _createCaption.getText().toString().trim();
                if (caption.isEmpty() || caption.equals("")){
                    Toast.makeText(EditActivity.this, "Caption can not be empty!", Toast.LENGTH_SHORT).show();
                }else {
                    _createCaption.setText("");
                    _editBar.setVisibility(View.GONE);
                    _tools.setVisibility(View.GONE);
                    hideKeyboard(EditActivity.this);
                }
            }
        });
    }


    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(Activity activity){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(_createCaption, InputMethodManager.RESULT_SHOWN);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

 /*   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) // Press Back Icon
        {finish();}
        return super.onOptionsItemSelected(item);
    }   */

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    //inflates menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.edit_meme_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Adding and Handling actions from toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_save:
                saveImage();
                return true;
            case R.id.action_share:
                if (memeSaved){
                    share(filename);
                }else{
                    saveImage();
                    share(filename);
                }
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //stores image to gallery
 /*   public void saveImage() {

        _editMeme.buildDrawingCache();
        Bitmap memeBitmap = _editMeme.getDrawingCache();

        String sdCard = Environment.getExternalStorageDirectory().toString();
        String filename = "MemeNoir "+String.format("%d.png", System.currentTimeMillis());

        FileOutputStream os = null;

        File file = new_tag File(sdCard, filename);

        //canvas with meme photo as background
        Canvas canvas = new_tag Canvas(memeBitmap);

        String topText= _topText.getText().toString();
        String botText = _bottomText.getText().toString();

        //Top text paint for drawing on canvas
        Paint topTextPaint = new_tag Paint();
        topTextPaint.setColor(_topText.getCurrentTextColor());
        //topTextPaint.setTypeface(impact_font);
        topTextPaint.setTextAlign(Paint.Align.CENTER);
        topTextPaint.setTextSize(topTextSize);
        topTextPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        //Bottom text paint for drawing on canvas
        Paint bottomTextPaint = new_tag Paint();
        bottomTextPaint.setColor(_bottomText.getCurrentTextColor());
        //textPaint.setTypeface(impact_font);
        bottomTextPaint.setTextAlign(Paint.Align.CENTER);
        bottomTextPaint.setTextSize(bottomTextSize);
        bottomTextPaint.setStyle(Paint.Style.FILL_AND_STROKE);


        //top and bottom text positioned as seen as in the preview
        canvas.drawText(topText, canvas.getWidth()/2, 100, topTextPaint);
        canvas.drawText(botText, canvas.getWidth()/2, 900, bottomTextPaint);

        //remember to enable storage permission on phone
        try {
            os = new_tag FileOutputStream(file);
            memeBitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            Toast.makeText(this, "Image saved successfully.", Toast.LENGTH_LONG).show();
        }
        catch(IOException e) {
            Toast.makeText(this, "Unable to save image (check app storage permissions)." , Toast.LENGTH_LONG).show();
            Log.e(this.getLocalClassName(), e.toString());
        }
    }   */

    public static Bitmap getScreenShot(View view){
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public void saveImage(){
        hideKeyboard(this);
        if(mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            interstitialShown = true;
        }
        try {
            Bitmap bm = getScreenShot(_lay);
            filename = "memeNoir_" + System.currentTimeMillis() + ".png";
            store(bm, filename);
        }catch (Exception e){
            Toast.makeText(this, "Something went wrong, please choose another image!", Toast.LENGTH_LONG).show();
        }

        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if(mInterstitialAd.isLoaded()){
                    mInterstitialAd.show();
                }
            }
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if (memeSavedNow) {
                    Toast.makeText(EditActivity.this, "Your Image has been saved to 'Saved Meme'", Toast.LENGTH_LONG).show();
                    memeSavedNow = false;
                }
            }
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (memeSavedNow) {
                    Toast.makeText(EditActivity.this, "Image saved successfully to 'Saved Meme'.", Toast.LENGTH_LONG).show();
                    memeSavedNow = false;
                }
            }
        });
    }

    public void share(final String filename){
        hideKeyboard(this);
        doShare(filename);
        if (mInterstitialAdVideo.isLoaded() && !interstitialShown) {
            mInterstitialAdVideo.show();
        }
    }

    public void doShare(String filename){
        //String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/memeNoir";
        Uri uri = Uri.fromFile(new File(dirPath, filename));
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        try{
            startActivity(Intent.createChooser(intent, "Share via"));
        }catch (Exception e){
            Toast.makeText(this, "No sharing app find", Toast.LENGTH_SHORT).show();
        }
    }

    public void store(Bitmap bm, String filename){
        dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/memeNoir";
        File dir = new File(dirPath);
        if (!dir.exists()){
            dir.mkdir();
        }
        File file = new File(dirPath, filename);
        try {
            FileOutputStream fos = null;
            fos = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG,100,fos);
            fos.flush();
            fos.close();
            memeSaved = true;
            memeSavedNow = true;
            //MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            if(!mInterstitialAd.isLoaded()) {
                Toast.makeText(EditActivity.this, "Image saved successfully to 'Saved Meme'.", Toast.LENGTH_LONG).show();
            }
        }catch (FileNotFoundException f){
            f.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mAdViewBanner.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdViewBanner.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdViewBanner.destroy();
    }

    /*  public void shareBitmapToOtherApp(Bitmap bitmap) {
        File file = new_tag File(getCacheDir(), getString(R.string.cached_picture_filename));
        File imageFile = ContextUtils.get().writeImageToFileJpeg(file, bitmap);
        if (imageFile != null) {
            Uri imageUri = FileProvider.getUriForFile(this, getString(R.string.app_fileprovider), imageFile);
            if (imageUri != null) {
                Intent shareIntent = new_tag Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.setDataAndType(imageUri, getContentResolver().getType(imageUri));
                shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
                startActivity(Intent.createChooser(shareIntent, getString(R.string.main__share_meme_prompt)));
            }
        }
    }   */
}
