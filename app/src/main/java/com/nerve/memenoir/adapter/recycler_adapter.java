package com.nerve.memenoir.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nerve.memenoir.EditActivity;
import com.nerve.memenoir.MainActivity;
import com.nerve.memenoir.R;
import com.nerve.memenoir.models.ImageUploadInfo;

import java.io.File;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Anumightytm on 12/12/2017.
 */

public class recycler_adapter extends RecyclerView.Adapter<recycler_adapter.ViewHolder> {

    Context context;
    String category;
    List<ImageUploadInfo> MainImageUploadInfoList;

    public recycler_adapter(Context context, List<ImageUploadInfo> TempList, String category) {

        this.MainImageUploadInfoList = TempList;
        this.context = context;
        this.category = category;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ImageUploadInfo UploadInfo = MainImageUploadInfoList.get(position);
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        //holder.imageNameTextView.setText(UploadInfo.getImageName());
        ImageLoader.getInstance().displayImage(UploadInfo.getImageURL(), holder.imageView);
            if (UploadInfo.getTimestamp() != null) {
                Long timestampPlusTwelve = Long.valueOf(UploadInfo.getTimestamp()) + (12 * 60 * 60 * 1000);
                if (System.currentTimeMillis() < timestampPlusTwelve) {
                    if (!appSharedPrefs.getBoolean(category+UploadInfo.getImageName(),false)){
                        holder._new.setVisibility(View.VISIBLE);
                    }else{
                        holder._new.setVisibility(View.GONE);
                    }

                }else{
                    holder._new.setVisibility(View.GONE);
                }
                //holder._new.setVisibility(View.VISIBLE);
            }else{
                holder._new.setVisibility(View.GONE);
            }
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public ImageView _new;
       // public TextView imageNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            _new = (ImageView) itemView.findViewById(R.id._new);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
          //  imageNameTextView = (TextView) itemView.findViewById(R.id.ImageNameTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position  =   getAdapterPosition();
                    final ImageUploadInfo UploadInfo = MainImageUploadInfoList.get(position);
                    SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                    prefsEditor.putBoolean(category+UploadInfo.getImageName(), true);
                    prefsEditor.apply();
                    _new.setVisibility(View.GONE);
                    //Pass the image title and url to DetailsActivity
                    Intent intent = new Intent(context, EditActivity.class);
                    intent.putExtra("name", UploadInfo.getImageName());
                    intent.putExtra("image", UploadInfo.getImageURL());
                    //Start details activity
                    context.startActivity(intent);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final SharedPreferences pref = context.getSharedPreferences("Memenoir", MODE_PRIVATE);
                    //If not admin, check for user's admin status
                    if (pref.getBoolean("isAdmin", false)) {
                        final int position = getAdapterPosition();
                        try {
                            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                            dialogBuilder.setTitle("Delete!");
                            dialogBuilder.setMessage("Are you sure you want to delete image?");
                            dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        DatabaseReference unwantedImage = FirebaseDatabase.getInstance().getReference("Meme").child(category).child(MainImageUploadInfoList.get(position).getKey());
                                        unwantedImage.removeValue();
                                        dialog.dismiss();
                                    }catch (Exception e){
                                        Toast.makeText(context, "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            dialogBuilder.create().show();
                        }catch (Exception e){
                            Toast.makeText(context, "SOmething went wrong, please try again!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    return false;
                }
            });
        }
    }
}
