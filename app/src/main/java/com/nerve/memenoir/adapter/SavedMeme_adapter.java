package com.nerve.memenoir.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nerve.memenoir.EditActivity;
import com.nerve.memenoir.R;
import com.nerve.memenoir.ViewMeme;
import com.nerve.memenoir.models.ImageUploadInfo;

import java.util.List;

/**
 * Created by Anumightytm on 12/22/2017.
 */

public class SavedMeme_adapter extends RecyclerView.Adapter<SavedMeme_adapter.ViewHolder> {

    Context context;
    List<String> MainImageUploadInfoList;

    public SavedMeme_adapter(Context context, List<String> TempList) {

        this.MainImageUploadInfoList = TempList;

        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String UploadInfo = MainImageUploadInfoList.get(position);

        //holder.imageNameTextView.setText(UploadInfo.getImageName());
        try {
            Bitmap myBitmap = BitmapFactory.decodeFile(MainImageUploadInfoList.get(position));
            holder.imageView.setImageBitmap(myBitmap);
        }catch (Exception e){}
    }

    @Override
    public int getItemCount() {

        return MainImageUploadInfoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        // public TextView imageNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            //  imageNameTextView = (TextView) itemView.findViewById(R.id.ImageNameTextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position  =   getAdapterPosition();
                    final String UploadInfo = MainImageUploadInfoList.get(position);
                    //Pass the image title and url to DetailsActivity
                    Intent intent = new Intent(context, ViewMeme.class);
                    intent.putExtra("image", UploadInfo);
                    //Start details activity
                    context.startActivity(intent);
                }
            });
        }
    }
}

