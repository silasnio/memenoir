package com.nerve.memenoir.Blog.Data;

import android.content.Context;
import android.text.format.DateUtils;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Anumightytm on 12/7/2017.
 */

public class MyDate {
        public static String getFormattedDate(Context context, long date) {
            Context weakCxt = new WeakReference<>(context).get();
            return DateUtils.getRelativeDateTimeString(weakCxt, date, DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS,
                    DateUtils.FORMAT_ABBREV_ALL).toString();
        }
        public static long getLongDate(String rawDate) {
            try {
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                Date date = inputFormat.parse(rawDate);
                return date.getTime();

            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }
}
