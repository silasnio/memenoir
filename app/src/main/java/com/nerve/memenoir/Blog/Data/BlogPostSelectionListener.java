package com.nerve.memenoir.Blog.Data;

/**
 * Created by anumighty on 12/08/17.
 */
public interface BlogPostSelectionListener {
    void onBlogPostSelected(Post post);
}
