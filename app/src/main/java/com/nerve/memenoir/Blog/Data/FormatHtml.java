/**
 * Created by Wilberforce on 03/22/17.
 */
package com.nerve.memenoir.Blog.Data;

import android.support.v7.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;

public class FormatHtml {
    public static String getFormattedHtml(String bodyHTML, String featuredImg, AppCompatActivity context) {
        WeakReference<AppCompatActivity> reference = new WeakReference<>(context);
        String head = "<head><style type='text/css'>" +
                getCSS(reference.get()) +
                "       </style></head>";
        return "<html>" + head + "<body style='margin:0;padding:0;'>" + "<p><img src=\"" + featuredImg + "\"/></p>" + bodyHTML + "</body></html>";
    }

    private static String getCSS(AppCompatActivity activity) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            InputStream cssString = activity.getAssets().open("css/style.css");
            reader = new BufferedReader(new InputStreamReader(cssString, "UTF-8"));
            String cssStyle;
            while ((cssStyle = reader.readLine()) != null) {
                builder.append(cssStyle);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }
}
