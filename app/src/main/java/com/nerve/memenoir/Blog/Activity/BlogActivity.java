package com.nerve.memenoir.Blog.Activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.nerve.memenoir.R;

public class BlogActivity extends AppCompatActivity {

    AppBarLayout appBarLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);

        toolbar = (Toolbar)findViewById(R.id.blog__toolbar);
        toolbar.setTitle("Blog");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_48px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlogActivity.this.onBackPressed();
            }
        });

        //Auto launch Blog Fragment
        android.support.v4.app.FragmentManager fragmentManager = BlogActivity.this.getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        BlogFragment fragment = new BlogFragment();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }
}
