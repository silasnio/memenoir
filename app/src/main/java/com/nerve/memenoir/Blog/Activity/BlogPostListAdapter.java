package com.nerve.memenoir.Blog.Activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.nerve.memenoir.App;
import com.nerve.memenoir.Blog.Data.AdapterBottomListener;
import com.nerve.memenoir.Blog.Data.AdaptersBottomViewHolder;
import com.nerve.memenoir.Blog.Data.BlogPostSelectionListener;
import com.nerve.memenoir.Blog.Data.Post;
import com.nerve.memenoir.Blog.Utils.ActivityUtils;
import com.nerve.memenoir.R;

import java.util.List;

/**
 * Created by Anumighty on 12/08/17.
 */
public class BlogPostListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<Post> posts;
    private BlogPostSelectionListener selectionListener;
    private AdapterBottomListener bottomListener;
    private int lastPosition = -1;
    private final int VIEW_POST = 0;
    private final int VIEW_PROG = 1;
    private AdaptersBottomViewHolder bottomViewHolder;

    public BlogPostListAdapter(List<Post> posts){
        this.posts = posts;
        this.selectionListener = null;
        this.bottomListener = null;
    }

    public void setSelectionListener (BlogPostSelectionListener selectionListener) {
        this.selectionListener = selectionListener;
    }

    public void setBottomListener (AdapterBottomListener bottomListener) {
        this.bottomListener = bottomListener;
    }

    public void processBottomViews(ActivityUtils.ResponseStatus status) {
        if (bottomViewHolder != null) {
            bottomViewHolder.processViews(status);
        }
    }

    @Override
    public int getItemViewType(int position) {
        try{
        if (isPosition(position)) {
            return VIEW_POST;
        } else {
            return VIEW_PROG;
        }
        }catch (Exception e){
            Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }
        return VIEW_PROG;
    }

    private boolean isPosition(int position) {
        return position != getItemCount()-1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_POST) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.blog_list_item, parent, false);
            return new BlogPostListHolder(v, selectionListener);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.post_bottom_layout, parent, false);
            bottomViewHolder =  new AdaptersBottomViewHolder(v, bottomListener);
            return bottomViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BlogPostListHolder) {
            ((BlogPostListHolder) holder).bindModel(posts.get(position));
            setAnimation(holder.itemView, position);


//        int commentCount = post.getDiscussion().getCommentCount();
//        if(commentCount == 0){
//            holder.blogCommentCountTV.setText("No Comments");
//        }else if(commentCount == 1){
//            holder.blogCommentCountTV.setText(commentCount+ " Comment");
//        }else{
//            holder.blogCommentCountTV.setText(commentCount + " Comments");
//        }
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // Animate only when scrolling down
        if (position > lastPosition) {
            TranslateAnimation animation = new TranslateAnimation(0, 0, 50, 0);
            animation.setDuration(400);
            viewToAnimate.startAnimation(animation);
        }
        lastPosition = position;
    }

    @Override
    public int getItemCount() {
        return posts == null ? (0) : posts.size()+1;
    }

    @Override
    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder holder) {
        holder.itemView.clearAnimation();
    }

}