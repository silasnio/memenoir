package com.nerve.memenoir.Blog.Activity;


import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nerve.memenoir.App;
import com.nerve.memenoir.Blog.Data.AdapterBottomListener;
import com.nerve.memenoir.Blog.Data.BlogEndpoint;
import com.nerve.memenoir.Blog.Data.BlogPostSelectionListener;
import com.nerve.memenoir.Blog.Data.EndlessRecyclerViewScrollListener;
import com.nerve.memenoir.Blog.Data.MyVolleyError;
import com.nerve.memenoir.Blog.Data.Post;
import com.nerve.memenoir.Blog.Data.VolleyCache;
import com.nerve.memenoir.Blog.Utils.ActivityUtils;
import com.nerve.memenoir.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class BlogFragment extends Fragment {

    //Constants
    private final String KEY_POST_ITEMS = "postItems";

    //Views
    private RecyclerView recyclerView;
    private ProgressBar mProgressBar;
    private LinearLayout errorLayout;
    private TextView errorText;
    private Button retryButton;


    //Fields
    private List<Post> mPostItemList;
    private BlogPostListAdapter adapter;
    private JsonObjectRequest objectRequest;
    private View view;
    private LinearLayoutManager mLayoutManager;
    private boolean hasFailed = false;
    private String savedData;
    private String errorMessage;
    private int pageNumber = 2;
    private boolean isMoreLoading = false;

    public BlogFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            hasFailed = savedInstanceState.getBoolean("hasFailed");
            errorMessage = savedInstanceState.getString("errorMessage");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_blog, container, false);
        bindViews();
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_POST_ITEMS)) {
            mPostItemList = savedInstanceState.getParcelableArrayList(KEY_POST_ITEMS);
            savedData = savedInstanceState.getString("SAVEDDATA");

        } else if (hasFailed) {

            errorText.setText(errorMessage);
            errorLayout.setVisibility(View.VISIBLE);
            mPostItemList = new ArrayList<>();
        } else {

            //Initializing the postlist
            mPostItemList = new ArrayList<>();
            getPostArray(false);
        }

        setUpWidgets();
        widgetListeners();

        return view;
    }

    private void bindViews() {
        recyclerView =  (RecyclerView) view.findViewById(R.id.post_recycler);
        mProgressBar = (ProgressBar) view.findViewById(R.id.post_pro_bar);
        errorLayout = (LinearLayout) view.findViewById(R.id.blog_info_error_root);
        errorText = (TextView) view.findViewById(R.id.blog_info_error_text);
        retryButton = (Button) view.findViewById(R.id.blog_info_error_button);
    }

    private void setUpWidgets() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new BlogPostListAdapter(mPostItemList);
        recyclerView.setAdapter(adapter);
        mProgressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity
                (), R.color.accent), PorterDuff.Mode.SRC_IN);
    }

    private void widgetListeners() {
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorLayout.setVisibility(View.GONE);
                getPostArray(false);
            }
        });


        //Add the scroll listener
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener( mLayoutManager) {
            @Override
            public void onLoadMore(final int page, int totalItemsCount, RecyclerView view) {
                if (!isMoreLoading) {
                    getPostArray(true);
                }
            }
        });

        adapter.setSelectionListener(new BlogPostSelectionListener() {
            @Override
            public void onBlogPostSelected(Post post) {
                BlogPostDetailActivity.mPost = post;
                Intent intent = new Intent(getActivity(), BlogPostDetailActivity.class);
                startActivity(intent);
            }
        });

        adapter.setBottomListener(new AdapterBottomListener() {
            @Override
            public void onLoadMoreClicked() {
                getPostArray(true);
            }
        });

    }

    //This method will get data from the blog
    private void getPostArray(final boolean isLoadMorePosts){
        String url;
        if (isLoadMorePosts) {
            url =  BlogEndpoint.getMoreBlogPost(getPageNumber());
        } else {
            url = BlogEndpoint.getAllBlogPost();
        }


        if (!isLoadMorePosts) {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        if (isLoadMorePosts) {
            isMoreLoading = true;
            processBottomViews(ActivityUtils.ResponseStatus.PRE_REQUEST);
        }

        //Creating a json request
        objectRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (!isLoadMorePosts) {
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.GONE);
                        try {
                            if (adapter.getItemCount() == 1) {
                                parsePostArray(response, false);
                            }
                        }catch (Exception e){
                            Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    savedData = response.toString();
                    hasFailed = false;

                }

                if (isLoadMorePosts) {
                    isMoreLoading = false;
                    processBottomViews(ActivityUtils.ResponseStatus.SUCCESS);
                    parsePostArray(response, true);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (!isLoadMorePosts ) {
                    if (mProgressBar != null) {

                        mProgressBar.setVisibility(View.GONE);
                    }
                    try{
                    if (adapter.getItemCount() == 1) {

                        errorMessage = MyVolleyError.errorMessage(error, getActivity());
                        errorText.setText(errorMessage);
                        errorLayout.setVisibility(View.VISIBLE);
                        hasFailed = true;
                    }
                    }catch (Exception e){
                        Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
                    }
                }

                if (isLoadMorePosts) {
                    isMoreLoading = false;
                    setPageNumber(getPageNumber() - 1);
                    processBottomViews(ActivityUtils.ResponseStatus.FAILED);
                }
            }
        }){
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                Response<JSONObject>  resp = super.parseNetworkResponse(response);
                return Response.success(resp.result, VolleyCache.parseIgnoreCacheHeaders(response, 7884000000L)); // 3 months cache
            }
        };

        int timeOut = 5000;
        RetryPolicy policy = new DefaultRetryPolicy(timeOut, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        objectRequest.setShouldCache(true);
        Activity a = (Activity) getContext();
        App.getInstance().getRequestQueue().add(objectRequest);

    }

    private void processBottomViews(ActivityUtils.ResponseStatus status) {
        adapter.processBottomViews(status);
    }

    private void parsePostArray(JSONObject object, boolean isLoadMorePosts) {
        try {
            final JSONArray array = object.getJSONArray("posts");
            if (!array.isNull(0)) {
                if (!isLoadMorePosts && !mPostItemList.isEmpty()) {
                    int dataSize = mPostItemList.size();
                    mPostItemList.clear();
                    adapter.notifyItemRangeRemoved(0, dataSize);
                }

                for(int i = 0; i<array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Post post = new Post(jsonObject);
                    mPostItemList.add(post);

                    if (isLoadMorePosts) {
                        mPostItemList.addAll(post);
                    }
                }

            } else {
                if (!isLoadMorePosts) {
                    errorText.setText(R.string.empty_search_result);
                    errorLayout.setVisibility(View.VISIBLE);
                    retryButton.setVisibility(View.GONE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try{
        if (isLoadMorePosts) {
            int curSize = adapter.getItemCount();
            adapter.notifyItemRangeChanged(curSize, mPostItemList.size()-1);
        } else {
            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
        }
        }catch (Exception e){
            Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (savedData != null) {
            savedInstanceState.putParcelableArrayList(KEY_POST_ITEMS, (ArrayList<? extends Parcelable>) mPostItemList);
            savedInstanceState.putString("SAVEDDATA", savedData);
        }
        savedInstanceState.putBoolean("hasFailed", hasFailed);
        savedInstanceState.putInt("postPage", pageNumber);
        savedInstanceState.putString("errorMessage", errorMessage);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            pageNumber = savedInstanceState.getInt("postPage");
        }
    }


    @Override
    public void onDestroyView() {
        if (objectRequest != null && !objectRequest.isCanceled()) {
            objectRequest.cancel();
        }
        super.onDestroyView();
    }


    public int getPageNumber() {
        return pageNumber++;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

}
