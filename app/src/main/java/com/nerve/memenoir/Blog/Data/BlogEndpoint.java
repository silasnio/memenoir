package com.nerve.memenoir.Blog.Data;

/**
 * Created by Anumightytm on 12/29/17.
 */
public class BlogEndpoint {

    public static final String BASE_URL = "https://public-api.wordpress.com/";

    public static final String BLOG_ID = "140917934";

    public static final String POST_ENDPOINT = "rest/v1.1/sites/"+BLOG_ID+"/posts/?number=10";

    public static String getAllBlogPost(){
        return BASE_URL + POST_ENDPOINT;
    }

    public static String getMoreBlogPost(int pageNumber) {
        return getAllBlogPost() + "&page=" + pageNumber;
    }

    public static String getCommentUrl(long postId) {
        return BASE_URL + "rest/v1.1/sites/" + BLOG_ID +  "/posts/" + postId + "/replies?number=100&page=1&order=ASC";
    }

    public static String getPostComment(long postID, String comment, String name, String email) {
        return "https://memenoir.com/blog/wp-json/wp/v2/comments?post="+postID+"&content="+comment+"&author_name="+name+"&author_email="+email;

    }
}
