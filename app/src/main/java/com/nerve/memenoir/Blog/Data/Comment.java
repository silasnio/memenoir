/*
 * Copyright (c) 2016-present, JadeByte Labs. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nerve.memenoir.Blog.Data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by Wilberforce on 28/05/2016.
 */
public class Comment implements Parcelable, Collection<Comment> {
    private String author;
    private String content;
    private String timesStamp;

    public Comment(JSONObject object) {
        try {
            JSONObject author = object.getJSONObject("author");
            setAuthor(author.getString("name"));

            SimpleDateFormat formatDate = new SimpleDateFormat("dd MMM, yy (HH:mm)", Locale.getDefault());
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String inputDateStr = object.getString("date");
            try {
                Date inputDate = inputFormat.parse(inputDateStr);
                String commDateStr = formatDate.format(inputDate);
                setTimesStamp(commDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            setContent(object.getString("content").replace("p>", "span>"));

        } catch (JSONException w) {
            w.printStackTrace();

        }
    }

    public String getAuthor() {
        return author;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    private void setContent(String content) {
        this.content = content;
    }

    public String getTimesStamp() {
        return timesStamp;
    }

    private void setTimesStamp(String timesStamp) {
        this.timesStamp = timesStamp;
    }


    @Override
    public boolean add(Comment object) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Comment> collection) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean contains(Object object) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @NonNull
    @Override
    public Iterator<Comment> iterator() {
        return null;
    }

    @Override
    public boolean remove(Object object) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @NonNull
    @Override
    public <T> T[] toArray(T[] array) {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.author);
        dest.writeString(this.content);
        dest.writeString(this.timesStamp);
    }

    protected Comment(Parcel in) {
        this.author = in.readString();
        this.content = in.readString();
        this.timesStamp = in.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
