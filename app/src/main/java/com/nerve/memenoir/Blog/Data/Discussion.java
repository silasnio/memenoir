package com.nerve.memenoir.Blog.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Anumighty on 12/07/17.
 */
public class Discussion implements Parcelable {
    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    private int commentCount;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.commentCount);
    }

    public Discussion() {
    }

    protected Discussion(Parcel in) {
        this.commentCount = in.readInt();
    }

    public static final Creator<Discussion> CREATOR = new Creator<Discussion>() {
        @Override
        public Discussion createFromParcel(Parcel source) {
            return new Discussion(source);
        }

        @Override
        public Discussion[] newArray(int size) {
            return new Discussion[size];
        }
    };
}
