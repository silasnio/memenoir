package com.nerve.memenoir.Blog.Data;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.nerve.memenoir.Blog.Utils.ActivityUtils;
import com.nerve.memenoir.R;


/**
 * Created by anumighty on 12/08/17.
 */

public class AdaptersBottomViewHolder extends RecyclerView.ViewHolder{
    private ProgressBar bottomProgress;
    private Button moreRetryButton;
    private AdapterBottomListener listener;

    public AdaptersBottomViewHolder(View itemView, final AdapterBottomListener listener) {
        super(itemView);
        setUpViews();
        this.listener = listener;
    }

    private void setUpViews() {
        bottomProgress = (ProgressBar) itemView.findViewById(R.id.post_bottom_progress);
        moreRetryButton = (Button) itemView.findViewById(R.id.post_error_button);
        bottomProgress.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor
                (itemView.getContext(), R.color.accent), PorterDuff.Mode.SRC_IN);

        moreRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                listener.onLoadMoreClicked();
            }
        });
    }

    public void processViews(ActivityUtils.ResponseStatus status) {
        if (status == ActivityUtils.ResponseStatus.FAILED) {
            bottomProgress.setVisibility(View.GONE);
            moreRetryButton.setVisibility(View.VISIBLE);

        } else if (status == ActivityUtils.ResponseStatus.SUCCESS) {
            bottomProgress.setVisibility(View.GONE);

        } else if (status == ActivityUtils.ResponseStatus.PRE_REQUEST) {
            bottomProgress.setVisibility(View.VISIBLE);
        }
    }
}
