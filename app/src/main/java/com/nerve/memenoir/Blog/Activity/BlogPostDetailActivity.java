package com.nerve.memenoir.Blog.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nerve.memenoir.App;
import com.nerve.memenoir.Blog.Data.BlogEndpoint;
import com.nerve.memenoir.Blog.Data.Comment;
import com.nerve.memenoir.Blog.Data.CommentAdapter;
import com.nerve.memenoir.Blog.Data.FormatHtml;
import com.nerve.memenoir.Blog.Data.MyConvert;
import com.nerve.memenoir.Blog.Data.MyDate;
import com.nerve.memenoir.Blog.Data.MyVolleyError;
import com.nerve.memenoir.Blog.Data.MyWebViewClient;
import com.nerve.memenoir.Blog.Data.Post;
import com.nerve.memenoir.Blog.Data.SimpleDividerItemDecoration;
import com.nerve.memenoir.Blog.Data.VolleyCache;
import com.nerve.memenoir.Blog.Utils.CommentFields;
import com.nerve.memenoir.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BlogPostDetailActivity extends AppCompatActivity {

    Toolbar mToolbar;
    TextView titleView;
    WebView contentView;
    CircleImageView authorAvatarView;
    TextView authorNameView;
    TextView postDateView;
    ScrollView scrollView;
    RecyclerView recyclerView;
    EditText nameField;
    EditText emailField;
    EditText commentField;
    LinearLayout commentParent;
    TextView commentTextView;
    Button displayComment;
    Button submitComment;
    ProgressBar progressBar;

    private final String KEY_COMMENTS = "commentItems";
    public static Post mPost;

    private ProgressDialog loadingDialog;
    private List<Comment> commentList;
    private String commentHeader;
    private CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_post_detail);
        getViewReferences();
        //setSupportActionBar(mToolbar);
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //customizeToolbar();
        titleView = (TextView) findViewById(R.id.blog_post_detail_title);
        contentView = (WebView) findViewById(R.id.blog_post_content);
        authorAvatarView = (CircleImageView) findViewById(R.id.post_author_avatar);
        authorNameView = (TextView) findViewById(R.id.post_author_name);
        postDateView = (TextView) findViewById(R.id.post_time);
        scrollView = (ScrollView) findViewById(R.id.post_content_scroll);
        mToolbar = (Toolbar) findViewById(R.id.blog__toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.comment_recycler);
        nameField = (EditText) findViewById(R.id.comment_name);
        emailField = (EditText) findViewById(R.id.comment_email);
        commentField = (EditText) findViewById(R.id.comment_content);
        commentParent = (LinearLayout) findViewById(R.id.comment_root);
        commentTextView = (TextView) findViewById(R.id.comment_header);
        displayComment = (Button) findViewById(R.id.post_comment_button);
        submitComment = (Button) findViewById(R.id.comment_submit_button);
        progressBar = (ProgressBar) findViewById(R.id.post_comment_progress);

        mToolbar.setTitle(mPost.getTitle().substring(0, 1).toUpperCase()
                + mPost.getTitle().substring(1));
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_48px));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlogPostDetailActivity.this.onBackPressed();
            }
        });

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_COMMENTS)) {
            commentList = savedInstanceState.getParcelableArrayList(KEY_COMMENTS);
        } else {
            commentList = new ArrayList<>();
        }

        if (savedInstanceState != null) {
            commentHeader = savedInstanceState.getString("commentHeader");
            commentTextView.setText(commentHeader);
        }
        updateUI();
    }


    private void getViewReferences() {

    }

    private void updateUI() {
        setUpWebView();

        int imgSize = (int) MyConvert.dpToPx(90, this);
        contentView.loadData(FormatHtml.getFormattedHtml(mPost.getContent(), mPost.getFeaturedImage(), this), "text/html; charset=UTF-8", null);
        ImageLoader.getInstance().displayImage(mPost.getAuthorAvatar(), authorAvatarView);
        //Picasso.with(App.getAppContext()).load(mPost.getAuthorAvatar()).
          //      placeholder(R.drawable.ic_author_large).centerCrop().resize(imgSize, imgSize).into(authorAvatarView);

        titleView.setText(mPost.getTitle());
        long longDate = MyDate.getLongDate(mPost.getDate());
        postDateView.setText(DateUtils.getRelativeDateTimeString(this,
                longDate, DateUtils.SECOND_IN_MILLIS, DateUtils.YEAR_IN_MILLIS,
                DateUtils.FORMAT_ABBREV_ALL).toString());
        authorNameView.setText(mPost.getAuthor());
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.accent), PorterDuff.Mode.SRC_IN);

        adapter = new CommentAdapter(commentList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView.setNestedScrollingEnabled(false);
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    private void setUpWebView() {
        WebSettings webSettings = contentView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        contentView.setWebViewClient(new MyWebViewClient(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }

        //Disabling scrolling of the webview
        contentView.setScrollContainer(false);
        contentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        scrollView.setSmoothScrollingEnabled(true);
        viewListeners();

        // Load comments after 1 second. This is to prevent the comment from showing up before the post details has been displayed
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadComments();
            }
        }, 1000);

    }

    private void viewListeners() {
        commentField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    submitComment();
                    return true;
                }
                return false;
            }
        });

        submitComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitComment();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blog_post_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }

    public void loadComments() {
        progressBar.setVisibility(View.VISIBLE);

        JsonObjectRequest commentRequest = new JsonObjectRequest(BlogEndpoint.getCommentUrl(mPost.getPostId()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseComment(response);
                        if (progressBar!= null) {
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                            displayComment.setVisibility(View.VISIBLE);
                        }
                    }
                }){
                    @Override
                    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                        Response<JSONObject>  resp = super.parseNetworkResponse(response);
                        return Response.success(resp.result, VolleyCache.parseIgnoreCacheHeaders(response, 7884000000L)); // 3 months cache
                    }
        };
        commentRequest.setTag("BlogPostDetailActivity");
        RetryPolicy policy = new DefaultRetryPolicy(7000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        commentRequest.setRetryPolicy(policy);
        commentRequest.setShouldCache(false);
        App.getInstance().getRequestQueue().add(commentRequest);
    }

    public void parseComment(JSONObject object) {
        int commentNum;
        try {
            commentNum = object.getInt("found");

            if (commentNum == 0) {
                commentTextView.setText(R.string.comment_no);
            } else if (commentNum == 1) {
               commentTextView.setText(R.string.one_comment);
            } else if (commentNum > 1) {
                commentTextView.setText(getString(R.string.num_comments, commentNum));
            }

            JSONArray array = object.getJSONArray("comments");
            for (int i = 0; i < array.length(); i++) {
                Comment comment = new Comment(array.getJSONObject(i));
                commentList.add(comment);

            }
        } catch (JSONException w) {
            w.printStackTrace();
        }

        try{
            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
        }catch (Exception e){
            Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }
        commentParent.setVisibility(View.VISIBLE);
    }

    public void submitComment() {
        if (allFieldsAreValid()) {
            String comment = null;
            try {
                comment = URLEncoder.encode(commentField.getText().toString().trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String name = null;
            try {
                name = URLEncoder.encode(getCommentName(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String email = null;
            try {
                email = URLEncoder.encode(emailField.getText().toString().trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            showLoadingDialog(false);
            String url = BlogEndpoint.getPostComment(mPost.getPostId(), comment, name, email);

            JsonObjectRequest postDetails = new JsonObjectRequest(Request.Method.POST, url,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseCommentResponse(response);
                            loadingDialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Log.d(TAG, "Error: " + error.getMessage());
                            NetworkResponse errorResponse = error.networkResponse;
                            if (errorResponse != null && errorResponse.data != null) {

                                String fullMessage;
                                switch (errorResponse.statusCode) {
                                    case 409:
                                        fullMessage = new String(errorResponse.data);
                                        loadingDialog.dismiss();
                                        parseCommentCode(fullMessage, 409);
                                        break;
                                    default:
                                        fullMessage = new String(errorResponse.data);
                                        loadingDialog.dismiss();
                                        parseCommentCode(fullMessage, errorResponse.statusCode);
                                }
                            } else {
                                loadingDialog.dismiss();
                                Toast.makeText(BlogPostDetailActivity.this, MyVolleyError.errorMessage(error, BlogPostDetailActivity.this), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            postDetails.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            postDetails.setShouldCache(false);
            App.getInstance().getRequestQueue().add(postDetails);

        }

    }

    private void parseCommentResponse(JSONObject object) {
        try {
            String status = object.getString("status");
            if (status.equalsIgnoreCase("hold")) {
                Toast.makeText(this, R.string.post_comment_mod, Toast.LENGTH_LONG).show();
            } else if (status.equalsIgnoreCase("approved")) {
                commentParent.setVisibility(View.GONE);
                int dataSize = commentList.size();

                if (!commentList.isEmpty()) {
                    commentList.clear();
                    adapter.notifyItemRangeRemoved(0, dataSize);
                }
                loadComments();
            }

            commentField.setText("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseCommentCode(String response, int statusCode) {
        Document document = Jsoup.parse(response);
        String body;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            body = String.valueOf(Html.fromHtml(document.select("body#error-page").html(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            body = String.valueOf(Html.fromHtml(document.select("body#error-page").html()));
        }

        commentField.setText("");
        if (statusCode == 409) {
            Toast.makeText(this, R.string.duplicate_comment, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, body, Toast.LENGTH_LONG).show();
        }
    }

    private boolean allFieldsAreValid() {
       return CommentFields.validate(this, emailField.getText(), commentField.getText());
    }

    private String getCommentName() {
        String name = nameField.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            return getString(R.string.anonymous);
        } else {
            return name;
        }
    }

    private void showLoadingDialog(boolean hide) {
        if (loadingDialog == null) {
            loadingDialog = new ProgressDialog(this);
            loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loadingDialog.setMessage(getString(R.string.post_comment));
            loadingDialog.setCancelable(false);
        }

        if (hide) {
            loadingDialog.hide();
        } else {
            loadingDialog.show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (commentHeader != null) {
            outState.putString("commentHeader", commentTextView.getText().toString());
        }

        try{
            if (adapter != null && adapter.getItemCount() > 0) {
                outState.putParcelableArrayList(KEY_COMMENTS, (ArrayList<? extends Parcelable>) commentList);
            }
        }catch (Exception e){
            Toast.makeText(App.getAppContext(), "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getInstance().getRequestQueue().cancelAll("BlogPostDetailActivity");
    }
}