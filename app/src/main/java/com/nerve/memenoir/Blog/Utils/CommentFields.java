/**
 * Created by William Wilberforce on 1/2/17.
 */
package com.nerve.memenoir.Blog.Utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import com.nerve.memenoir.R;

import java.lang.ref.WeakReference;


public class CommentFields {

    public static boolean validate (Context context, Editable emailAddress, Editable commentText) {
        Context weakContext = new WeakReference<>(context).get();
        String message = null;
        if (!isEmailValid(emailAddress)) {
            message = weakContext.getString(R.string.email);
        }

        if (!isCommentValid(commentText)) {
            message = message ==  null  ? weakContext.getString(R.string.comment) : weakContext.getString(R.string.and_comment, message);
        }

        if (TextUtils.isEmpty(message)) {
            return true;
        } else {
            String toastMsg = message.contains(weakContext.getString(R.string.email)) && message.contains(weakContext.getString(R.string.comment)) ?
                    weakContext.getString(R.string.are_invalid, message) : weakContext.getString(R.string.is_invalid, message);
            Toast.makeText(weakContext, toastMsg, Toast.LENGTH_LONG).show();
            return false;
        }

    }

    private static boolean isCommentValid (Editable commentText) {
        String comment = commentText.toString().trim();
        return comment.length() >= 2;
    }

    private static boolean isEmailValid(Editable emailAddress) {
        String email = emailAddress.toString().trim();
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        // return TextUtils.isEmpty(email) || Patterns.EMAIL_ADDRESS.matcher(email).matches();

        // Email is invalid on when it is not empty and doesn't match an Email address. If it's empty it's still valid
    }
}
