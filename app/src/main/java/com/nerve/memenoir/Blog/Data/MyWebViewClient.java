
/**
 * Created by Wilberforce on 03/22/17.
 */
package com.nerve.memenoir.Blog.Data;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nerve.memenoir.Blog.Utils.MyUrlHandler;

public class MyWebViewClient extends WebViewClient {
    private AppCompatActivity activity;

    public MyWebViewClient(AppCompatActivity activity) {
        this.activity = activity;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return handleUrl(Uri.parse(url));
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return handleUrl(request.getUrl());
    }

    private boolean handleUrl(Uri uri) {
        MyUrlHandler.consumeUrl(activity, uri.toString());
        // Returning true the webView should not load the url
        // e.g. open web page in a Browser or handle it locally
        return true;
    }
}
