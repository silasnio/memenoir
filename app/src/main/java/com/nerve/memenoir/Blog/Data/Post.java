package com.nerve.memenoir.Blog.Data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.Html;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Anumighty on 12/07/17.
 */

public class Post implements Parcelable, Collection<Post> {

    private long postId;
    private long siteId;
    private String author;
    private String date;
    private String modified;
    private String title;
    private String url;
    private String shortUrl;
    private String content;
    private String featuredImage;
    private String authorAvatar;
    private Discussion discussion;

    public Discussion getDiscussion() {
        return discussion;
    }

    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }


    public Post(){

    }
    public Post(JSONObject jsonObject){
        try {
            setPostId(jsonObject.getInt("ID"));
            setSiteId(jsonObject.getLong("site_ID"));

            JSONObject authorObj = jsonObject.getJSONObject("author");

//            Discussion discussion = new_tag Discussion();
//            discussion.setCommentCount(jsonObject.getInt("comment_count"));
//
//            setDiscussion(discussion);
            setAuthor(Html.fromHtml(authorObj.getString("name")).toString());
            setAuthorAvatar(authorObj.getString("avatar_URL"));
            setDate(jsonObject.getString("date"));
            setModified(jsonObject.getString("modified"));
            setTitle(Html.fromHtml(jsonObject.getString("title")).toString());
            setUrl(jsonObject.getString("URL"));
            setShortUrl(jsonObject.getString("short_URL"));
            setContent(jsonObject.getString("content"));
            setFeaturedImage(jsonObject.getString("featured_image"));

            //BlogPostDb.saveBlogPost(this);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public long getPostId() {
        return postId;
    }

    public void setPostId(int id) {
        this.postId = id;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.postId);
        dest.writeLong(this.siteId);
        dest.writeString(this.author);
        dest.writeString(this.date);
        dest.writeString(this.modified);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.shortUrl);
        dest.writeString(this.content);
        dest.writeString(this.featuredImage);
        dest.writeParcelable(this.discussion, flags);
    }

    protected Post(Parcel in) {
        this.postId = in.readLong();
        this.siteId = in.readLong();
        this.author = in.readString();
        this.date = in.readString();
        this.modified = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        this.shortUrl = in.readString();
        this.content = in.readString();
        this.featuredImage = in.readString();
        this.discussion = in.readParcelable(Discussion.class.getClassLoader());
    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @NonNull
    @Override
    public Iterator<Post> iterator() {
        return null;
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @NonNull
    @Override
    public <T> T[] toArray(@NonNull T[] a) {
        return null;
    }

    @Override
    public boolean add(Post post) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends Post> c) {
        return false;
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
