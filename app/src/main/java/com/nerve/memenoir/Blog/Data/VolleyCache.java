package com.nerve.memenoir.Blog.Data;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.Map;

/**
 * Created by Anumightytm on 12/7/2017.
 */

public class VolleyCache {
    /**
     * Ignores the cache headers from a response and caches the with the specified duration
     * @param response The response to cache
     * @param cacheDuration The duration to cache int in milliseconds
     * @return The cache entry
     */
    public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response, long cacheDuration) {
        long now = System.currentTimeMillis();

        Map<String, String> headers = response.headers;
        long serverDate = 0;
        String serverEtag;
        String headerValue;

        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }

        serverEtag = headers.get("ETag");

        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheDuration;

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        entry.ttl = ttl;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        return entry;
    }
}
