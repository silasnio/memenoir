package com.nerve.memenoir.Blog.Utils;

import android.app.Activity;

public class ActivityUtils {
    private ActivityUtils(Activity activity) {
        super();
    }

    public static final String TYPE_BLOG_HTML = "blogHtml";
    public static final String EXTERNAL_URL = "externalUrl";
    public static final String EXTERNAL_TYPE = "externalType";
    public static final String EXTERNAL_ID = "externalId";
    public static final String EXTERNAL_TITLE = "externalTitle";
    public static final String EXTERNAL_BODY = "externalBody";
    public static final String TYPE_BLOG = "blog";

    public static ActivityUtils get(Activity activity) {
        return new ActivityUtils(activity);
    }

    public enum ResponseStatus {FAILED, SUCCESS, PRE_REQUEST}
}
