package com.nerve.memenoir.Blog.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nerve.memenoir.Blog.Data.BlogPostSelectionListener;
import com.nerve.memenoir.Blog.Data.MyConvert;
import com.nerve.memenoir.Blog.Data.MyDate;
import com.nerve.memenoir.Blog.Data.Post;
import com.nerve.memenoir.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Anumighty on 12/08/17.
 */

public class BlogPostListHolder extends RecyclerView.ViewHolder{

    private final ImageView thumbView;
    private final TextView postTitle;
    private final TextView postDate;
    private final LinearLayout postItemRoot;
    private BlogPostSelectionListener selectionListener;
    private Context context;
    private int size;

    public BlogPostListHolder(View itemView, BlogPostSelectionListener selectionListener) {
        super(itemView);
        thumbView = (ImageView) itemView.findViewById(R.id.post_image);
        postTitle = (TextView) itemView.findViewById(R.id.post_title);
        postDate = (TextView) itemView.findViewById(R.id.post_date);
        postItemRoot = (LinearLayout) itemView.findViewById(R.id.post_item_root);
        this.selectionListener = selectionListener;
        this.context = itemView.getContext();
        size = (int) MyConvert.dpToPx(200, context);
    }

    public void bindModel(final Post post) {
        try {
            BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
            resizeOptions.inSampleSize = 3; // decrease size 3 times
            resizeOptions.inScaled = true;
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showStubImage(R.drawable.ic_launcher)
                    .showImageForEmptyUri(R.drawable.ic_launcher)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .decodingOptions(resizeOptions)
                    .postProcessor(new BitmapProcessor() {
                        @Override
                        public Bitmap process(Bitmap bmp) {
                            return Bitmap.createScaledBitmap(bmp, size, size, false);
                        }
                    })
                    .build();
            ImageLoader.getInstance().displayImage(post.getFeaturedImage(), thumbView, options);

            //Picasso.with(context).load(post.getFeaturedImage()).
                   // placeholder(R.drawable.ic_launcher).centerCrop().resize(size, size).into(thumbView);

            postTitle.setText(post.getTitle());
            postDate.setText(getFormattedDate(post.getDate()));

            postItemRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectionListener.onBlogPostSelected(post);
                }
            });
        }catch (Exception e){}
    }

    private String getFormattedDate(String rawDate) {
        long longDate = MyDate.getLongDate(rawDate);

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        return DateUtils.getRelativeTimeSpanString(longDate, date.getTime(),
                DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL).toString();
    }
}
