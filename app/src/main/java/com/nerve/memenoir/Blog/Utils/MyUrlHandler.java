package com.nerve.memenoir.Blog.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nerve.memenoir.App;
import com.nerve.memenoir.Blog.Activity.BlogPostDetailActivity;
import com.nerve.memenoir.Blog.Data.Post;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Anumightytm on 02/01/18.
 */

public class MyUrlHandler {
    private static AppCompatActivity context;
    private static String TAG = "MyUrlHandler";
    public static void consumeUrl(AppCompatActivity cxt, String link) {
        context = new WeakReference<>(cxt).get();
        try {
            String url = link.toLowerCase();
            Log.i(TAG, "consumeUrl: " + url);
            if (url.contains("/blog/")) {
                    Log.i(TAG, "consumeUrl: Blog");
                    startExternalUrlActivity(context, url, ActivityUtils.TYPE_BLOG_HTML);

                }
        }catch (Exception e){}
    }
    private static String getProductId (String url) {
        String[] splits = url.split("-");
        return splits[splits.length-1].toLowerCase().replace("p", "");
    }

    private static String getStoreAlias(String url) {
        String[] splits = url.split("/");
        return splits[splits.length-1];
    }

    private static void startExternalUrlActivity(Context context, String url, String type) {
        consumeUrl(url);
    }

    private static void consumeUrl(String url) {
        Log.i(TAG, "consumeUrl " + url);
        JsonObjectRequest objectRequest = new JsonObjectRequest(url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "onResponse");
                parserRequest(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse");
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("app", "NtczZTk4MDIxMTVlNjYx");
                headers.put("hash", "26f8d06051d593efb9623d54c12eb2f0b158df35");
                headers.put("Authorization", "Basic " + "ZnVubWluaXlpZWt1bmRheW9AZ21haWwuY29tOnF3ZXJ0eQ==");
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(7000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        objectRequest.setRetryPolicy(policy);
        objectRequest.setShouldCache(true);
        objectRequest.setTag(TAG);
        App.getInstance().getRequestQueue().add(objectRequest);
    }

    private static void parserRequest(JSONObject object) {
        Log.i(TAG, "parserRequest with " + object.toString());
        Log.d(TAG, "parserRequest: Blog");
        BlogPostDetailActivity.mPost = new Post(object);
        Intent intent = new Intent(context, BlogPostDetailActivity.class);
        context.startActivity(intent);
    }

}
