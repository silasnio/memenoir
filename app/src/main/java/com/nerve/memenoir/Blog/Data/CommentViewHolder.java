package com.nerve.memenoir.Blog.Data;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.nerve.memenoir.R;


public class CommentViewHolder extends RecyclerView.ViewHolder {
    private TextView name, content, timeStamp;

    public CommentViewHolder(View view) {
        super(view);
        name = (TextView) itemView.findViewById(R.id.comment_author);
        content = (TextView) itemView.findViewById(R.id.comment_content);
        timeStamp = (TextView) itemView.findViewById(R.id.comment_timestamp);
    }

    public void bindModel(final Comment comment) {
        name.setText(comment.getAuthor());
        content.setText(Html.fromHtml(comment.getContent()));
        timeStamp.setText(comment.getTimesStamp());

        int position = getAdapterPosition();
    }
}
