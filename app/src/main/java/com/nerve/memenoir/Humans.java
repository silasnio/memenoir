package com.nerve.memenoir;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nerve.memenoir.adapter.recycler_adapter;
import com.nerve.memenoir.models.ImageUploadInfo;
import com.nerve.memenoir.utils.GridDecoration;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//import com.nerve.memenoir.utils.GridDecoration;

/**
 * Created by Anumightytm on 12/12/2017.
 */

public class Humans extends Fragment {
    private RecyclerView _recyclerView;
    // Creating List of ImageUploadInfo class.
    List<ImageUploadInfo> list = new ArrayList<>();
    DatabaseReference databaseReference;
    RecyclerView.Adapter adapter ;
    ProgressDialog progressDialog;
    Boolean isHumanLoaded = false;
    SharedPreferences appSharedPrefs;
    private LinearLayout _emptyTemplate;

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_layout, container, false);

        _emptyTemplate = (LinearLayout)view.findViewById(R.id.main__activity__list_empty__layout);

            _recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            _recyclerView.setHasFixedSize(true);
            _recyclerView.setItemViewCacheSize(20);
            _recyclerView.setDrawingCacheEnabled(true);
            _recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            _recyclerView.addItemDecoration(new GridDecoration(1.7f));

            int gridColumns = getGridColumnCountPortrait();
            RecyclerView.LayoutManager recyclerGridLayout = new GridLayoutManager(getActivity(), gridColumns);

            _recyclerView.setLayoutManager(recyclerGridLayout);

            // Assign activity this to progress dialog.
        appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        isHumanLoaded = appSharedPrefs.getBoolean("humansLoaded",false);
        if (!isHumanLoaded) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading Meme Templates.");
            progressDialog.show();
        }

            // Setting up Firebase image upload folder path in databaseReference.
            databaseReference = FirebaseDatabase.getInstance().getReference().child("Meme").child("Humans");

                if (isHumanLoaded) {
                    loadFromMemory();
                }else{
                    ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                        //There is network, do nothing
                    }else{
                        _emptyTemplate.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }
                }
                    // Adding Add Value Event Listener to databaseReference.
                    databaseReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            try {
                                _emptyTemplate.setVisibility(View.GONE);
                                list.clear();
                                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                                    ImageUploadInfo imageUploadInfo = postSnapshot.getValue(ImageUploadInfo.class);
                                    imageUploadInfo.setKey(postSnapshot.getKey());
                                    list.add(imageUploadInfo);
                                }

                                Collections.reverse(list);

                                adapter = new recycler_adapter(getActivity(), list, "Humans");
                                _recyclerView.setAdapter(adapter);
                                //isHumanLoaded = true;
                                try {
                                    progressDialog.dismiss();
                                } catch (Exception e) {
                                }
                                if (list.size() > 0) {
                                    try {
                                        SharedPreferences appSharedPrefs = PreferenceManager
                                                .getDefaultSharedPreferences(getActivity());
                                        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                                        Gson gson = new Gson();
                                        String json = gson.toJson(list);
                                        prefsEditor.putString("Humans", json);
                                        prefsEditor.putBoolean("humansLoaded", true);
                                        prefsEditor.apply();
                                    } catch (Exception e) {
                                    }
                                }
                            }catch (Exception e){}
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            // Hiding the progress dialog.
                            //progressDialog.dismiss();

                        }
                    });

        return view;

    }

    public int getGridColumnCountPortrait() {
        int count = -1;
        if (count == -1) {
            count = 3 + (int) Math.max(0, 0.5 * (getEstimatedScreenSizeInches() - 5.0));
            //setGridColumnCountPortrait(count);
        }
        return count;
    }

    public double getEstimatedScreenSizeInches() {
        DisplayMetrics dm = this.getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y) * 1.16;  // 1.16 = est. Nav/Statusbar
        screenInches = screenInches < 4.0 ? 4.0 : screenInches;
        screenInches = screenInches > 12.0 ? 12.0 : screenInches;
        return screenInches;
    }

    public void loadFromMemory(){
        try {
            _emptyTemplate.setVisibility(View.GONE);
            Gson gson = new Gson();
            String json = appSharedPrefs.getString("Humans", "");
            Type type = new TypeToken<List<ImageUploadInfo>>() {
            }.getType();
            //list.clear();
            list = gson.fromJson(json, type);
            adapter = new recycler_adapter(getActivity(), list, "Humans");
            _recyclerView.setAdapter(adapter);
            //progressDialog.dismiss();
        }catch (Exception e){}
    }


}
