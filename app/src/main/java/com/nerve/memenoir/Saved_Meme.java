package com.nerve.memenoir;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nerve.memenoir.adapter.SavedMeme_adapter;
import com.nerve.memenoir.adapter.recycler_adapter;
import com.nerve.memenoir.models.ImageUploadInfo;
import com.nerve.memenoir.utils.GridDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anumightytm on 12/22/2017.
 */

public class Saved_Meme extends Fragment {

    private RecyclerView _recyclerView;
    ProgressDialog progressDialog;

    List<ImageUploadInfo> list = new ArrayList<>();
    RecyclerView.Adapter adapter ;
    ArrayList<String> f = new ArrayList<String>();// list of file paths
    File[] listFile;
    private TextView noFileText;
    private LinearLayout noFile;
    public static boolean isDeleted = false;

    public Saved_Meme() {
        // Required empty public constructor
    }


    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_layout, container, false);

        noFile = (LinearLayout)view.findViewById(R.id.main__activity__list_empty__layout);
        noFileText = (TextView)view.findViewById(R.id.main__activity__list_empty__text);

        _recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setItemViewCacheSize(20);
        _recyclerView.setDrawingCacheEnabled(true);
        _recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        _recyclerView.addItemDecoration(new GridDecoration(1.7f));

        int gridColumns = getGridColumnCountPortrait();
        RecyclerView.LayoutManager recyclerGridLayout = new GridLayoutManager(getActivity(), gridColumns);

        _recyclerView.setLayoutManager(recyclerGridLayout);

        // Assign activity this to progress dialog.
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading Meme Templates.");
        progressDialog.show();
        getFromSdcard();

        return view;

    }

    public void getFromSdcard()
    {
        File file= new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/memeNoir");

        if (file.isDirectory())
        {
            listFile = file.listFiles();
            if (listFile.length > 0) {
                for (int i = 0; i < listFile.length; i++) {
                    f.add(listFile[i].getAbsolutePath());
                }
                adapter = new SavedMeme_adapter(getContext(), f);
                _recyclerView.setAdapter(adapter);
                noFile.setVisibility(View.GONE);
                //isHumanLoaded = true;
                progressDialog.dismiss();
            }
        }
        if (!file.exists()){
            noFile.setVisibility(View.VISIBLE);
            noFileText.setText("No meme created/saved yet!");
        }
        progressDialog.dismiss();

    }


    public int getGridColumnCountPortrait() {
        int count = -1;
        if (count == -1) {
            count = 3 + (int) Math.max(0, 0.5 * (getEstimatedScreenSizeInches() - 5.0));
            //setGridColumnCountPortrait(count);
        }
        return count;
    }

    public double getEstimatedScreenSizeInches() {
        DisplayMetrics dm = this.getResources().getDisplayMetrics();

        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y) * 1.16;  // 1.16 = est. Nav/Statusbar
        screenInches = screenInches < 4.0 ? 4.0 : screenInches;
        screenInches = screenInches > 12.0 ? 12.0 : screenInches;
        return screenInches;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDeleted){
            f.clear();
            // Assign activity this to progress dialog.
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading Meme Templates.");
            progressDialog.show();
            getFromSdcard();
        }
    }
}
