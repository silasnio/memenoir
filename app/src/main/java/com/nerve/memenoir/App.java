package com.nerve.memenoir;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Anumightytm on 12/19/2017.
 */

public class App extends Application {
    private volatile static App app;
    private static Context sContext;
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler;
    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
        setAPPContext(getApplicationContext());

        // Do it on Application start for nostra image loader
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher) // resource or drawable
                .showImageForEmptyUri(R.drawable.ic_launcher) // resource or drawable
                .showImageOnFail(R.drawable.ic_launcher) // resource or drawable
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        /*Picasso.Builder builder = new_tag Picasso.Builder(this);
        builder.downloader(new_tag OkHttp3Downloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        //built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);*/

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });

    }

    public static Context getAppContext() {

        return app;

    }
    private void setAPPContext(Context appContext) {

        sContext = appContext;

    }

    public static App getInstance() {

        return AppHelper.INSTANCE;

    }
    public RequestQueue getRequestQueue() {

        return AppHelper.REQUEST_QUEUE;

    }

    private static class AppHelper {

        private static App INSTANCE = app;
        private static RequestQueue REQUEST_QUEUE = Volley.newRequestQueue(getAppContext());

    }

}
