package com.nerve.memenoir;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.nerve.memenoir.Saved_Meme.isDeleted;

public class ViewMeme extends AppCompatActivity {
    Toolbar toolbar;
    String viewSaved;
    ImageView _imageView;
    Boolean hidden = false;
    InterstitialAd mInterstitialAdVideo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_meme);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        try {
            assert actionBar != null;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        } catch (Exception ignored) {
        }

        try {
            _imageView = (ImageView) findViewById(R.id.memeView);
            viewSaved = getIntent().getStringExtra("image");
            Bitmap myBitmap = BitmapFactory.decodeFile(viewSaved);
            _imageView.setImageBitmap(myBitmap);
        }catch (Exception e){
            Toast.makeText(this, "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }

        _imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hidden) {
                    toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
                    hidden = true;
                }else{
                    toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
                    hidden = false;
                }
            }
        });

        //Admob Interstitial
        mInterstitialAdVideo = new InterstitialAd(this);
        mInterstitialAdVideo.setAdUnitId(getString(R.string.google_ad_id_intastitial_video));
        mInterstitialAdVideo.loadAd(new AdRequest.Builder().build());
    }

    public void share(){
        try {
            File f = new File(viewSaved);
            Uri uri = Uri.parse("file://" + f.getAbsolutePath());
            Intent share = new Intent(Intent.ACTION_SEND);
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.putExtra(Intent.EXTRA_SUBJECT, "MemeNoir");
            share.putExtra(Intent.EXTRA_TEXT, "Meme shared from MemeNoir");
            share.setType("image/*");
            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(share, "Share meme via"));
        }catch (Exception e){
            Toast.makeText(this, "Something went wrong, please try again!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_memeview, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Adding and Handling actions from toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.action_share:
                share();
                if (mInterstitialAdVideo.isLoaded()) {
                    mInterstitialAdVideo.show();
                }
                return true;
            case R.id.action_delete:
                delete();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void delete(){
        File file = new File(viewSaved);
        file.delete();
        isDeleted = true;
        Toast.makeText(this, "Image deleted!", Toast.LENGTH_SHORT).show();
        finish();
    }
}
