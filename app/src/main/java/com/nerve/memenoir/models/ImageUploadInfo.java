package com.nerve.memenoir.models;

/**
 * Created by Anumightytm on 12/12/2017.
 */

public class ImageUploadInfo {

    public String imageName;
    public String imageURL;
    public String key;
    public String timestamp;

    public ImageUploadInfo() {

    }

    public ImageUploadInfo(String name, String url, String timestamp) {
        this.timestamp = timestamp;
        this.imageName = name;
        this.imageURL= url;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}