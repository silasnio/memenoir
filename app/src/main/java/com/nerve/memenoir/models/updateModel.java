package com.nerve.memenoir.models;

/**
 * Created by Anumightytm on 1/30/2018.
 */

public class updateModel {
    public String versionCode;
    public String whatsNew;
    public String url;

    public updateModel(){

    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getWhatsNew() {
        return whatsNew;
    }

    public void setWhatsNew(String whatsNew) {
        this.whatsNew = whatsNew;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
