package com.nerve.memenoir;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by Anumightytm on 2/6/2018.
 */

public class SquareImageView2 extends AppCompatImageView {

    public SquareImageView2(Context context) {
        super(context);
    }

    public SquareImageView2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    // override to set the height of the Image View to its width
    @Override
    @SuppressWarnings("SuspiciousNameCombination")
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        // Optimization so we don't measure twice unless we need to
        if (width != height) {
            setMeasuredDimension(width / 2, width / 2);
        }else{
            setMeasuredDimension(width / 3, width);
        }
    }
}
