package com.nerve.memenoir;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nerve.memenoir.models.ImageUploadInfo;

import java.io.IOException;

public class Upload extends AppCompatActivity {

    Toolbar toolbar;
    Spinner _category;
    EditText _imageName;
    ImageView _add, _imageUpload;
    Button _upload;
    CheckBox sendNotification;
    String TemplateCategory = "empty", imageName;
    int Image_Request_Code = 2;
    Uri FilePathUri;
    StorageReference storageReference;
    DatabaseReference databaseReference, databaseReference2;
    ProgressDialog progressDialog ;
    private static final String[]paths = {"Choose Category","Humans","Animals", "Cartoon","Others"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        try {
            assert actionBar != null;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        } catch (Exception ignored) {
        }

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Meme");
        databaseReference2 = FirebaseDatabase.getInstance().getReference();

        _category = (Spinner)findViewById(R.id.category);
        _imageName = (EditText)findViewById(R.id.imageName);
        _add = (ImageView)findViewById(R.id.add);
        _imageUpload = (ImageView)findViewById(R.id.imageUpload);
        _upload = (Button)findViewById(R.id.upload);
        sendNotification = (CheckBox)findViewById(R.id.checkbox);

        ArrayAdapter<String> _cat = new ArrayAdapter<String>(Upload.this, android.R.layout.simple_spinner_item,paths);
        _cat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        _category.setAdapter(_cat);
        _category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!parent.getItemAtPosition(position).toString().equals("Choose Category")) {
                    TemplateCategory = parent.getItemAtPosition(position).toString();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        _add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageFromGallery();
            }
        });

        _imageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageFromGallery();
            }
        });

        _upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadImage();
            }
        });
    }

    public void addImageFromGallery(){
        Intent intent = new Intent();
        // Setting intent type as image to select image from phone storage.
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {
                // Getting selected image into Bitmap.
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);
                _imageUpload.setImageBitmap(bitmap);
                _add.setVisibility(View.GONE);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Creating Method to get the selected image file Extension from File Path URI.
    public String GetFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;
    }

    // Creating UploadImageFileToFirebaseStorage method to upload image on storage.
    public void UploadImage() {
        // Checking whether FilePathUri Is empty or not.
        if (FilePathUri != null) {
            // Setting progressDialog Title.
            progressDialog = new ProgressDialog(Upload.this);
            progressDialog.setTitle("Uploading image...");
            progressDialog.show();

            if (TemplateCategory.equals("empty")) {
                Toast.makeText(this, "Choose a category", Toast.LENGTH_SHORT).show();
            } else {
                imageName = _imageName.getText().toString().trim();
                if (imageName.equals("")) {
                    Toast.makeText(this, "Enter image name", Toast.LENGTH_SHORT).show();
                } else {
                    StorageReference storageReference2nd = storageReference.child(TemplateCategory+"/"+imageName+"_"+System.currentTimeMillis());
                    // Adding addOnSuccessListener to second StorageReference.
                    storageReference2nd.putFile(FilePathUri)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Hiding the progressDialog after done uploading.
                                    progressDialog.dismiss();

                                    // Showing toast message after done uploading.
                                    Toast.makeText(getApplicationContext(), "Image Uploaded Successfully!", Toast.LENGTH_LONG).show();
                                    String timestamp = String.valueOf(System.currentTimeMillis());

                                    @SuppressWarnings("VisibleForTests")
                                    ImageUploadInfo imageUploadInfo = new ImageUploadInfo(imageName, taskSnapshot.getDownloadUrl().toString(), timestamp);

                                    // Adding image upload id s child element into databaseReference.
                                    databaseReference.child(TemplateCategory).push().setValue(imageUploadInfo);
                                    //send notification if checkbox is checked
                                    if (sendNotification.isChecked()) {
                                        databaseReference2.child("Notification").push().setValue(imageUploadInfo);
                                    }
                                    _imageUpload.setImageResource(R.color.ash);
                                    _add.setVisibility(View.VISIBLE);
                                    _imageName.setText("");
                                    _category.setSelection(getIndex(_category, "Choose Category"));
                                }
                            })
                            // If something goes wrong .
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                    // Hiding the progressDialog.
                                    progressDialog.dismiss();

                                    // Showing exception erro message.
                                    Toast.makeText(Upload.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            })

                            // On progress change upload time.
                            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                                    // Setting progressDialog Title.
                                    progressDialog.setTitle("Image is Uploading...");

                                }
                            });
                }
            }

        }
        else {

            Toast.makeText(Upload.this, "Please Select Image or Add Image Name", Toast.LENGTH_LONG).show();

        }
    }

    private int getIndex(Spinner spinner, String myString){
        int index = 0;
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.finish();
        return;
    }
}
